import React, { Component } from "react";
// import "./src/App.css";
import "../src/App.css";
import { Icon } from "@material-ui/core";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "",
      resp: [],
      showDetails: 0,
      repo: []
    };
    this.get_name = this.get_name.bind(this);
    this.search_name = this.search_name.bind(this);
    this.showDets = this.showDets.bind(this);
    this.hideDets = this.hideDets.bind(this);
    this.sort = this.sort.bind(this);
  }

  get_name(event) {
    this.setState({ value: event.target.value });
  }

  hideDets() {
    this.setState({
      showDetails: 0
    });
  }

  showDets() {
    this.setState({
      showDetails: 1
    });
  }

  search_name() {
    fetch("https://api.github.com/search/users?q=" + this.state.value, {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => response.json())
      .then(responseJSON => {
        this.setState({
          resp: responseJSON,
          gotData: 1
        });
      });

    fetch(`https://api.github.com/users/${this.state.value}/repos`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(repos => {
        this.setState({
          repo: repos,
          gotRepo: 1
        });
      });
  }

  sort(event) {
    const sortJsonArray = require("sort-json-array");
    if (event.target.value === "asc") {
      sortJsonArray(this.state.resp.items, "login", "asc");
      this.setState({
        resp: this.state.resp
      });
    } else if (event.target.value === "desc") {
      sortJsonArray(this.state.resp.items, "login", "des");
      this.setState({
        resp: this.state.resp
      });
    }
  }

  render() {
    return (
      <div className="App" style={{ width: "100%" }}>
        <div
          style={{
            backgroundColor: "#0072ff",
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-around",
            height: "73px"
          }}
        >
          {this.state.gotData === 1 && (
            <select
              style={{ height: "23px", marginTop: "23px" }}
              onChange={this.sort}
              defaultValue={this.state.sort_value}
            >
              <option>Sort by</option>
              <option value="asc">Sort by userName(A - Z)</option>
              <option value="desc">Sort by userName(Z - A)</option>
            </select>
          )}
          <div style={{ marginTop: "23px" }}>
            <input
              type="text"
              name="name"
              id="s_name"
              placeholder="Enter UserName"
              onChange={this.get_name}
            />
            {/* <button type="submit" onClick={this.search_name}> */}
            <Icon
              className="fa fa-search"
              style={{ height: "20px", color: "#fff" }}
              onClick={this.search_name}
            >
              search
            </Icon>
            {/* </button> */}
          </div>
        </div>
        {this.state.gotData === 1 && this.state.resp.items.length > 0 && (
          <div style={{ width: "50vw", marginLeft: "55px", fontSize: "13px" }}>
            <p style={{ fontFamily: "monospace" }}>
              Total Count: {this.state.resp.items.length}
            </p>
          </div>
        )}
        <div
          style={{
            width: "50vw",
            backgroundColor: "#eee",
            marginLeft: "323px"
          }}
        >
          {this.state.gotData === 1 &&
            this.state.resp.items.map((item, index) => (
              <div
                key={item.id}
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "center",
                  borderBottom: "42px solid white",
                  height: "auto"
                }}
              >
                <img
                  alt="user_image"
                  style={{ height: "26vh", marginTop: "25px" }}
                  src="https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_250,h_300/https://www.imanami.com/wp-content/uploads/2016/03/unknown-user.jpg"
                ></img>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "end",
                    marginLeft: "23px"
                  }}
                >
                  <p style={{ fontFamily: "monospace" }}>
                    UserName: {item.login}
                  </p>
                  <p style={{ fontFamily: "monospace" }}>
                    Profile Url: {item.html_url}
                  </p>
                  {this.state.showDetails === 1 &&
                    this.state.gotRepo === 1 &&
                    this.state.repo.length > 0 &&
                    this.state.repo.map((rep, i) => (
                      <div key={i} style={{ width: "60%" }}>
                        <p
                          style={{ fontSize: "12px", fontFamily: "monospace" }}
                        >
                          Repo name: {rep.name}
                        </p>
                        <p
                          style={{ fontSize: "12px", fontFamily: "monospace" }}
                        >
                          Language: {rep.language}
                        </p>
                      </div>
                    ))}
                  {this.state.showDetails === 1 &&
                    this.state.repo.length <= 0 && (
                      <p style={{ fontFamily: "monospace" }}>
                        No Repo Available
                      </p>
                    )}
                </div>
                {this.state.showDetails === 0 && (
                  <button
                    style={{
                      height: "27px",
                      marginTop: "180px",
                      backgroundColor: "#fff",
                      color: "#0072ff",
                      borderRadius: "7px",
                      marginLeft: "43px"
                    }}
                    onClick={this.showDets}
                  >
                    Show Details
                  </button>
                )}
                {this.state.showDetails === 1 && (
                  <button
                    style={{
                      height: "27px",
                      marginTop: "180px",
                      backgroundColor: "#fff",
                      color: "#0072ff",
                      borderRadius: "7px",
                      marginLeft: "43px"
                    }}
                    onClick={this.hideDets}
                  >
                    Hide Details
                  </button>
                )}
              </div>
            ))}
        </div>
      </div>
    );
  }
}

export default App;
