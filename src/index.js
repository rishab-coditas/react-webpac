import React from "react";
import ReactDOM from "react-dom";
import App from "../components/App";
import "./index.css";
import "bootstrap/dist/css/bootstrap.min.css";
import * as Sentry from "@sentry/browser";
Sentry.init({
  dsn: "https://b03e2ee8497f4dbaa6ee4f65cd1351c1@sentry.io/1760970"
});

ReactDOM.render(<App />, document.getElementById("root"));
